import os
import sys
from MacAddr import MacAddr

try:
	# VMware
	response = MacAddr.Info("00-50-56-D2-F2-08")

	if isinstance(response, dict):
		if "error" not in response["result"].keys():

			#Displays all information
			#print(response)

			# Displays some interesting information
			print("Company : {}".format(response["result"]["company"]))
			print("Country : {}".format(response["result"]["country"]))
except:
	pass

if sys.platform.startswith('win32'):
	os.system("pause")
