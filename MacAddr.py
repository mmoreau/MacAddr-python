import urllib.request as url
import json
import codecs


class MacAddr:


	def __init__(self):
		pass


	@classmethod
	def Info(cls, mac):

		if isinstance(mac, str):

			try:
				request = url.Request(f"https://macvendors.co/api/{mac}", headers={'User-Agent' : "Googlebot"})
				response = url.urlopen(request)
				reader = codecs.getreader("utf-8")
				obj = json.load(reader(response))

				return obj
			except:
				pass